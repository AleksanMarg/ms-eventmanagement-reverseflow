package ru.sberbank.riskmetrics.constants;

/**
 * {Хранение строковых констант}
 */
public class StringConst {
    public static final String EXECUTE_REQUESTER = "sitnov@lanit.ru";
    public static final String EXECUTE_RISK_SERVICE_RISK_SEGMENT_TYPE = "RS";
    public static final String EXECUTE_RISK_SERVICE_RISK_SEGMENT_SCENARIO = "RF";
    public static final String EXECUTE_RELATED_ENTITY_TYPE = "RISKMETRICS";

    public static final String RISK_METRIC_TYPE_RISK_SEGMENT = "1";
    public static final String RISK_METRIC_STATUS_APPROVED = "APPROVED";
    public static final String RISK_METRIC_BUSINESS_PROCESS_SOURCE_SYSTEM_ID_RISK_SEGMENT = "PegaComponentRiskSegment";
}
