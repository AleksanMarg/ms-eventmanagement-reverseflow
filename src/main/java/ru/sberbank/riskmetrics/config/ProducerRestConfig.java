package ru.sberbank.riskmetrics.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.policy.TimeoutRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import java.io.File;
import java.io.FileInputStream;
import java.net.URI;
import java.net.URL;
import java.security.KeyStore;
import java.time.Duration;
import java.util.Arrays;

@Configuration
@Slf4j
public class ProducerRestConfig {

    @Bean("primary")
    public RestTemplate restTemplatePega(
            @Value("${conf.ssl.key-store-location}") String keystorePath,
            @Value("${conf.ssl.key-store-password}") char[] keystorePassword,

            @Value("${conf.ssl.trust-store-location}") String truststorePath,
            @Value("${conf.ssl.trust-store-password}") char[] truststorePassword,

            @Value("${conf.pega.request.connect-timeout}") long connTimeout,
            @Value("${conf.pega.request.read-timeout}") long readTimeout,

            RestTemplateBuilder builder
    ) {

        HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();

        try (FileInputStream keystoreInputStream = new FileInputStream(new File(new URI(keystorePath)));
             FileInputStream truststoreInputStream = new FileInputStream(new File(new URI(truststorePath)))) {

            KeyStore keyStore = KeyStore.getInstance("jks");
            keyStore.load(keystoreInputStream, keystorePassword);

            KeyStore trustStore = KeyStore.getInstance("jks");
            trustStore.load(truststoreInputStream, truststorePassword);

            SSLContext sslContext = SSLContextBuilder.create()
                    .setProtocol("TLS")
                    .loadKeyMaterial(keyStore, keystorePassword) // Пароль для ключа не используем, но можно указать пароль keystore
                    .loadTrustMaterial(new URL(truststorePath), truststorePassword)
                    .build();

            httpClientBuilder.setSSLSocketFactory(new SSLConnectionSocketFactory(sslContext));

        } catch (Exception ex) {
            log.error("Error creating SSL socket factory for RestTemplate", ex);

        } finally {
            Arrays.fill(keystorePassword, (char) 0);
            Arrays.fill(truststorePassword, (char) 0);
        }

        CloseableHttpClient httpClient = httpClientBuilder.build();

        HttpComponentsClientHttpRequestFactory requestFactory =
                new HttpComponentsClientHttpRequestFactory(httpClient);

        return builder
                .setConnectTimeout(Duration.ofMillis(connTimeout))
                .setReadTimeout(Duration.ofMillis(readTimeout))
                .requestFactory(() -> requestFactory)
                .build();
    }

    /**
     * Если сервис на стороне Пеги не отвечает, этот RetryTemplate будет производить повторную попытку запроса.
     * Пауза (back off) между попытками растет в геометрической прогрессии для уменьшения нагрузки, если сервис
     * недоступен долгое время.
     * Попытки прекращаются после истечения таймаута.
     *
     * @param initialInterval Начальное время паузы
     * @param maxInterval     Максимальное время паузы
     * @param multiplier      Знаменатель прогрессии
     * @param timeoutMinutes  Таймаут в минутах
     */
    @Bean
    public RetryTemplate retryTemplatePega(
            @Value("${conf.pega.request.retry.interval-initial}") long initialInterval,
            @Value("${conf.pega.request.retry.interval-max}") long maxInterval,
            @Value("${conf.pega.request.retry.interval-multiplier}") double multiplier,
            @Value("${conf.pega.request.retry.timeout-minutes}") long timeoutMinutes
    ) {

        RetryTemplate retryTemplate = new RetryTemplate();

        ExponentialBackOffPolicy backOffPolicy = new ExponentialBackOffPolicy();
        backOffPolicy.setInitialInterval(initialInterval);
        backOffPolicy.setMaxInterval(maxInterval);
        backOffPolicy.setMultiplier(multiplier);
        retryTemplate.setBackOffPolicy(backOffPolicy);

        TimeoutRetryPolicy retryPolicy = new TimeoutRetryPolicy();
        retryPolicy.setTimeout(Duration.ofMinutes(timeoutMinutes).toMillis());
        retryTemplate.setRetryPolicy(retryPolicy);

        return retryTemplate;
    }
}
