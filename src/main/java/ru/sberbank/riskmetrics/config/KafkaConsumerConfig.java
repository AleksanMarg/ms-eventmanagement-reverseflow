package ru.sberbank.riskmetrics.config;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import ru.sberbank.riskmetrics.data.consumer.kafka.IncomingEvent;

import java.util.HashMap;
import java.util.Map;

@Configuration
@Slf4j
@Data
public class KafkaConsumerConfig {

    @Value("${BOOTSTRAP_SERVERS}")                                      private String bootstrapServers;
    @Value("${KAFKA_SSL_ENABLE}")                                       private String groupId;
    @Value("${spring.kafka.client-id}")                                 private String clientId;
    @Value("${spring.kafka.ssl-enable}")                                private Boolean sslEnabled;
    @Value("${spring.kafka.consumer.enable-auto-commit}")               private String enableAutoCommit;
    @Value("${spring.kafka.consumer.properties.max.poll.interval.ms}")  private String maxPullIntervalMs;
    @Value("${spring.kafka.consumer.auto-offset-reset}")                private String offSet;
    @Value("${spring.kafka.properties.security.protocol}")              private String securityProtocol;

    @Value("${spring.kafka.properties.ssl.endpoint.identification.algorithm}")
    private String identificationAlgorithm;

    //JKS key store
    @Value("${spring.kafka.ssl.trust-store-location}")                  private String trustStoreLocation;
    @Value("${spring.kafka.ssl.trust-store-password}")                  private String trustStorePassword;
    @Value("${spring.kafka.ssl.key-store-location}")                    private String keyStoreLocation;
    @Value("${spring.kafka.ssl.key-store-password}")                    private String keyStorePassword;

    @Bean
    public ConsumerFactory<String, IncomingEvent> consumerFactory() {
        Map<String, Object> props = new HashMap<>();

        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        props.put(ConsumerConfig.CLIENT_ID_CONFIG, clientId);
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, enableAutoCommit);
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, offSet);
        props.put(ConsumerConfig.MAX_POLL_INTERVAL_MS_CONFIG, maxPullIntervalMs);

        if (sslEnabled) {
            props.put("ssl.endpoint.identification.algorithm",       identificationAlgorithm);

            props.put("security.protocol",       securityProtocol);
            props.put("ssl.truststore.location", trustStoreLocation);
            props.put("ssl.truststore.password", trustStorePassword);

            props.put("ssl.key.password", keyStorePassword);
            props.put("ssl.keystore.password", keyStorePassword);
            props.put("ssl.keystore.location", keyStoreLocation);
        }

        return new DefaultKafkaConsumerFactory<>(props,
                new StringDeserializer(),
                new JsonDeserializer<>(IncomingEvent.class));
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, IncomingEvent> kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, IncomingEvent> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        factory.getContainerProperties().setAckMode(ContainerProperties.AckMode.RECORD);
        return factory;
    }


}
