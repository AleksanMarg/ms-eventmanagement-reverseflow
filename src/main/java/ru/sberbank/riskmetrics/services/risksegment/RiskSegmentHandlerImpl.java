package ru.sberbank.riskmetrics.services.risksegment;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sberbank.riskmetrics.constants.StringConst;
import ru.sberbank.riskmetrics.data.consumer.kafka.IncomingEvent;
import ru.sberbank.riskmetrics.data.producer.pega.ExecuteRq;
import ru.sberbank.riskmetrics.data.producer.pega.body.BusinessProcess;
import ru.sberbank.riskmetrics.data.producer.pega.body.RiskObject;
import ru.sberbank.riskmetrics.data.producer.pega.body.RiskService;
import ru.sberbank.riskmetrics.services.Handler;
import ru.sberbank.riskmetrics.services.ProducerRestService;

import java.util.ArrayList;
import java.util.List;

import static ru.sberbank.riskmetrics.constants.StringConst.*;

@Component
@Slf4j
public class RiskSegmentHandlerImpl implements Handler {

    @Autowired
    ProducerRestService producerRestService;
    private String sourceSystemId;
    private static final String INITIAL_TESTS_FAILED = ": Initial tests FAILED!";

    public void processing(IncomingEvent incomingEvent) {

        //Логирование и проверки
        log.info(RiskSegmentHandlerImpl.class + ": Cheking input data from EXECUTE: ");

        if (incomingEvent.getBody().getRiskMetric().getObjectId().isEmpty()) {
            log.error(RiskSegmentHandlerImpl.class + ": getRiskMetric().getObjectId().isEmpty() = true");
            log.error(RiskSegmentHandlerImpl.class + INITIAL_TESTS_FAILED);
            return;
        }

        if (incomingEvent.getBody().getRiskMetric().getStatus().isEmpty()) {
            log.error(RiskSegmentHandlerImpl.class + ": getRiskMetric().getStatus().isEmpty() = true");
            log.error(RiskSegmentHandlerImpl.class + INITIAL_TESTS_FAILED);
            return;
        }
        String riskMetricStatus = incomingEvent.getBody().getRiskMetric().getStatus();

        if (!(riskMetricStatus.equals(RISK_METRIC_STATUS_APPROVED))) {
            log.info(RiskSegmentHandlerImpl.class + ": No handler for such RiskMetric status");
            log.error(RiskSegmentHandlerImpl.class + INITIAL_TESTS_FAILED);
            return;
        } else {
            log.info(RiskSegmentHandlerImpl.class + ": RiskMetricStatus = " + riskMetricStatus);
        }
        if (incomingEvent.getBody().getRiskMetric().getBusinessProcess().getSourceSystemId().isEmpty()) {
            log.error(RiskSegmentHandlerImpl.class + ": getRiskMetric().getBusinessProcess().getSourceSystemId().isEmpty() = true");
            log.error(RiskSegmentHandlerImpl.class + INITIAL_TESTS_FAILED);
            return;
        }
        sourceSystemId = incomingEvent.getBody().getRiskMetric().getBusinessProcess().getSourceSystemId();

        if ((sourceSystemId.equals(RISK_METRIC_BUSINESS_PROCESS_SOURCE_SYSTEM_ID_RISK_SEGMENT))) {
            log.info(RiskSegmentHandlerImpl.class + ": No handler for such RiskMetric.BusinessProcess.SourceSystemId");
            log.error(RiskSegmentHandlerImpl.class + INITIAL_TESTS_FAILED);
            return;
        } else {
            log.info(RiskSegmentHandlerImpl.class + ": SourceSystemId = " + sourceSystemId);
        }
        log.info(RiskSegmentHandlerImpl.class + ": Initial tests passed. OK!");

        // Если первичные проверки прошли, то идём дальше
        try {
            log.info(RiskSegmentHandlerImpl.class + ": Sending EXECUTE to PEGA");
            ExecuteRq executeRq = prepareMessageExecute(incomingEvent);
            producerRestService.sendingExecute(executeRq);
        } catch (Exception ex) {
            log.error("Handler caught exception! Error: ", ex);
        }
    }

    // описание отправки Execute в пегу
    private ExecuteRq prepareMessageExecute(IncomingEvent incomingEvent) {
        ExecuteRq executeRq = new ExecuteRq();

        executeRq.setRequestor(incomingEvent.getBody().getRiskMetric().getApproveUser().getId());
        String dateTime = incomingEvent.getBody().getRiskMetric().getCalcDateTime();
        dateTime = dateTime.replaceAll("-","");
        dateTime = dateTime.replaceAll(":","");
        dateTime = dateTime.replace("Z", ".000 GMT");

        RiskService riskService = RiskService.builder()
                .type(StringConst.EXECUTE_RISK_SERVICE_RISK_SEGMENT_TYPE)
                .scenario(StringConst.EXECUTE_RISK_SERVICE_RISK_SEGMENT_SCENARIO)
                .status(dateTime).build();

        RiskObject riskObject = RiskObject.builder()
                .id(incomingEvent.getBody().getRiskMetric().getRiskObject().getSourceId())
                .type(incomingEvent.getBody().getRiskMetric().getType()).build();

        BusinessProcess businessProcess = BusinessProcess.builder().id(incomingEvent.getBody().getRiskMetric().getValue())
                .type(sourceSystemId).build();

        List<RiskObject> riskObjectsList = new ArrayList<>();
        riskObjectsList.add(riskObject);

        executeRq.setRiskService(riskService);
        executeRq.setRiskObject(riskObjectsList);
        executeRq.setBusinessProcess(businessProcess);
        return executeRq;
    }
}