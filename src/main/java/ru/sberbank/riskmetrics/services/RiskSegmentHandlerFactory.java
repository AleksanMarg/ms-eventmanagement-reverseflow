package ru.sberbank.riskmetrics.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.sberbank.riskmetrics.services.risksegment.RiskSegmentHandlerImpl;

@Component
public class RiskSegmentHandlerFactory implements HandlerFactory {

    @Autowired
    ApplicationContext applicationContext;
    @Autowired
    ProducerRestService producerRestService;

    public Handler getHandlerService() {
        return applicationContext.getBean(RiskSegmentHandlerImpl.class);
    }
}
