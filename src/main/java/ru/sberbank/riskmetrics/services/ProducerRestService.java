package ru.sberbank.riskmetrics.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.retry.annotation.Retryable;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.client.RestTemplate;
import ru.sberbank.riskmetrics.data.producer.pega.ExecuteRq;
import ru.sberbank.riskmetrics.data.response.pega.NotifyResponsePega;
import ru.sberbank.riskmetrics.services.utils.EntityJsonUtils;

@Service
@Slf4j
@Retryable
public class ProducerRestService {

    private final RestTemplate restTemplatePega;
    private final RetryTemplate retryTemplatePega;
    private String pegaEndpoint;

    public ProducerRestService(
            @Qualifier("primary") RestTemplate restTemplatePega,
            RetryTemplate retryTemplatePega,
            @Value("#{'${conf.pega.host}' + '${conf.pega.execute-endpoint}'}") String pegaEndpoint) {

        this.restTemplatePega = restTemplatePega;
        this.retryTemplatePega = retryTemplatePega;
        this.pegaEndpoint = pegaEndpoint;
    }

    public void sendingExecute(final ExecuteRq event) {
        Assert.notNull(event, "Execute request cannot be null");

        retryTemplatePega.execute((retryContext) -> {

            log.info("retry template send to pega: attempt " + retryContext.getRetryCount());
            log.info("retry template send to pega: last exception in retryContext: " + retryContext.getLastThrowable());

            try {
                sendToPega(event);
            } catch (Exception ex) {
                log.error("sendToPega threw exception: ", ex);
                throw ex;
            }
            return null;
        });
    }

    private void sendToPega(final ExecuteRq executeRq) {
        try {
            String event = EntityJsonUtils.marshal(executeRq);

            log.info("ExecuteRq message json: " + event);

            NotifyResponsePega response = restTemplatePega.postForObject(pegaEndpoint, event, NotifyResponsePega.class);

            String responseMessage = EntityJsonUtils.marshal(response);
            log.info("ExecuteRs message json: " + responseMessage);

            if (response != null && response.getMessage().size() == 1) {
                if (response.getMessage().get(0).getCode().equals("CRM")
                        && response.getMessage().get(0).getSeverity().equalsIgnoreCase("ERROR")) {
                    // Ошибка от компонента
                    log.error("Код ошибки - 1, Для " + executeRq.getRiskObject().get(0).getId() + " ошибка сохранения: " + response.getMessage().get(0).getText());
                } else if (response.getMessage().get(0).getCode().equals("0")
                        && response.getMessage().get(0).getSeverity().equalsIgnoreCase("SUCCESS")) {
                    log.info("Код ошибки - 0, Для " + executeRq.getRiskObject().get(0).getId() + " успешно сохранен риск-сегмент в CRM");
                } else {
                    throw new RuntimeException("Invalid response from PEGA: "
                            + response);
                }
            } else {
                throw new RuntimeException("Non-success status from Pega: "
                        + (response == null ? "null" : response));
            }

        } catch (Exception ex) {
            log.error("Error while sending event to execute pega: ", ex);
            throw ex;
        }
    }
}
