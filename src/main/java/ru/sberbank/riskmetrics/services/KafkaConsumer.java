package ru.sberbank.riskmetrics.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.listener.KafkaListenerErrorHandler;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Service;
import ru.sberbank.riskmetrics.constants.StringConst;
import ru.sberbank.riskmetrics.data.consumer.kafka.IncomingEvent;
import ru.sberbank.riskmetrics.services.risksegment.RiskSegmentHandlerImpl;
import ru.sberbank.riskmetrics.services.utils.EntityJsonUtils;

@Slf4j
@Service
@EnableKafka
@RequiredArgsConstructor
public class KafkaConsumer {

    private Handler handlersService;
    private HandlerFactory handlerFactory;
    private final ApplicationContext applicationContext;

    @KafkaListener(topics = "${conf.kafka.risk-metric-topic-name}", errorHandler = "listenerErrorHandler")
    public void processMessageRS(IncomingEvent incomingEvent, MessageHeaders headers) {

        log.info("RISKS METRIC Ver 1.1.1");

        try {
            String eventString = EntityJsonUtils.marshal(incomingEvent);
            String headersString = EntityJsonUtils.marshal(headers);
            log.info("INCOMING HEADERS " + headersString);
            log.info("INCOMING EVENT " + eventString);
        } catch (Exception exception) {
            log.info("Код ошибки - 2, Ошибка чтения RiskMetricEvent.json");
            return;
        }

        log.info("Handler work STARTED");

        handlerFactory = defineHandlerFactory(incomingEvent);

        if (handlerFactory == null) {
            return;
        } else {
            handlersService = handlerFactory.getHandlerService();
            handlersService.processing(incomingEvent);
        }

        log.info("Handler work ENDED");
    }

    private HandlerFactory defineHandlerFactory(IncomingEvent incomingEvent) {
        if (incomingEvent.getBody().getRiskMetric().getType().isEmpty()) {
            log.error(RiskSegmentHandlerImpl.class + ": Initial tests FAILED!");
            return null;
        }
        if (!(incomingEvent.getBody().getRiskMetric().getType().equals(StringConst.RISK_METRIC_TYPE_RISK_SEGMENT))) {
            log.info(RiskSegmentHandlerImpl.class + ": No handler for such RiskMetric type");
            log.error(RiskSegmentHandlerImpl.class + ": Initial tests FAILED!");
            return null;
        } else {
            log.info(RiskSegmentHandlerImpl.class + ": RiskMetricType = " + incomingEvent.getBody().getRiskMetric().getType());
            return applicationContext.getBean(RiskSegmentHandlerFactory.class);
        }
    }

    @Bean
    public KafkaListenerErrorHandler listenerErrorHandler() {
        return (message, exception) -> {
            log.error("Kafka listener completed with exception: ", exception);
            return null;
        };
    }
}
