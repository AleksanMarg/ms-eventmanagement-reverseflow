package ru.sberbank.riskmetrics.services;

import ru.sberbank.riskmetrics.data.consumer.kafka.IncomingEvent;

public interface Handler {
    void processing(IncomingEvent incomingEvent);
}
