package ru.sberbank.riskmetrics.services;

public interface HandlerFactory {
    Handler getHandlerService();
}
