package ru.sberbank.riskmetrics.data.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown=true)
public class Message {
   private String code;
   private String severity;
   private String text;
   private List<EmbeddedMessage> embeddedMessage;
}
