package ru.sberbank.riskmetrics.data.response.pega;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import ru.sberbank.riskmetrics.data.producer.pega.body.BusinessProcess;
import ru.sberbank.riskmetrics.data.producer.pega.body.RiskService;
import ru.sberbank.riskmetrics.data.response.Message;

import java.util.List;

/**
 * Ответ от Pega-компонента "Управление событиями"
 */
@Data
@JsonIgnoreProperties(ignoreUnknown=true)
public class NotifyResponsePega {

    private BusinessProcess businessProcess;
    private List<Message> message;
    private RiskService riskService;
    private Task task;


}
