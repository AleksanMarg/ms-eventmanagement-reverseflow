package ru.sberbank.riskmetrics.data.riskmetric;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;


@Data
@JsonIgnoreProperties(ignoreUnknown=true)
public class Decision {
    private List<String> reason;
    private String comment;
    private String type;
}
