package ru.sberbank.riskmetrics.data.riskmetric;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;


@Data
@JsonIgnoreProperties(ignoreUnknown=true)
public class ExecutedStrategy {
    private String objectId;
}
