package ru.sberbank.riskmetrics.data.riskmetric;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;


@Data
@JsonIgnoreProperties(ignoreUnknown=true)
public class Model {
    private String name;
    private String id;
}
