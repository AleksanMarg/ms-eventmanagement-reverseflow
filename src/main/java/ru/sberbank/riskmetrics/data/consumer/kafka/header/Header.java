package ru.sberbank.riskmetrics.data.consumer.kafka.header;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown=true)
public class Header {
    private String evtType;
    private String evtVersion;
    private String srcModule;
    private String evtId;
    private Long evtDate;
    private Long sndDate;
    private String parentID;
    private String prevEvtID;
}
