package ru.sberbank.riskmetrics.data.consumer.kafka.body;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import ru.sberbank.riskmetrics.data.riskmetric.*;

@Data
@JsonIgnoreProperties(ignoreUnknown=true)
public class RiskMetric {

    private String objectId;
    private String sourceId;
    private String sourceSystemId;
    private BusinessProcess businessProcess;
   // @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.S'Z'")
  //  private Date dateTime;
    private String riskServiceId;
    private RiskObject riskObject;
    private String type;
    private String value;
    private String status;
    private String calcId;
    private String calcDateTime;
    private CalcUser calcUser;
    private String calcType;
    private String calcComment;
    private Model model;
    private ExecutedStrategy executedStrategy;
    private String approveDateTime;
    private ApproveUser approveUser;
    private String expiryDate;
    private FinReport finReport;
    private Decision decision;
    private String saveDateTime;
}
