package ru.sberbank.riskmetrics.data.consumer.kafka.body;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown=true)
public class Body {
    private RiskMetric riskMetric;
}
