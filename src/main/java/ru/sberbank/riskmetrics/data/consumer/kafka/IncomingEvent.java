package ru.sberbank.riskmetrics.data.consumer.kafka;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import ru.sberbank.riskmetrics.data.consumer.kafka.body.Body;
import ru.sberbank.riskmetrics.data.consumer.kafka.header.Header;

@Data
@JsonIgnoreProperties(ignoreUnknown=true)
public class IncomingEvent {
    private Header header;
    private Body body;
}
