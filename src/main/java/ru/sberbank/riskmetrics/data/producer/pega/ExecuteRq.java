package ru.sberbank.riskmetrics.data.producer.pega;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import ru.sberbank.riskmetrics.data.producer.pega.body.BusinessProcess;
import ru.sberbank.riskmetrics.data.producer.pega.body.RiskObject;
import ru.sberbank.riskmetrics.data.producer.pega.body.RiskService;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown=true)
public class ExecuteRq {
    private RiskService riskService;
    private List<RiskObject> riskObject;
    private BusinessProcess businessProcess;
    private String requestor;
}
