package ru.sberbank.riskmetrics.data.producer.pega.body;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown=true)
public class BusinessProcess {
    private String id;
    private String type;
}
