package ru.sberbank.riskmetrics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RiskMetricsEventApplication {

    public static void main(String[] args) {
        SpringApplication.run(RiskMetricsEventApplication.class, args);
    }

}
