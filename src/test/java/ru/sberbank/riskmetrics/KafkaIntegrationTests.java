package ru.sberbank.riskmetrics;


//@SpringBootTest
//@EmbeddedKafka(topics = "${conf.kafka.ack-topic-name}", partitions = 1)
//@TestPropertySource(properties = {
//        "spring.kafka.consumer.bootstrap-servers=${spring.embedded.kafka.brokers}",
//        "spring.kafka.consumer.auto-offset-reset=earliest"})
class KafkaIntegrationTests {
//
//    @MockBean
//    @Qualifier("secondary")
//    private RestTemplate restTemplatePega;
//
//    @Autowired
//    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") // Баг Intellij
//    private EmbeddedKafkaBroker embeddedKafkaBroker;
//
//    @Autowired
//    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") // Баг Intellij
//    private KafkaListenerEndpointRegistry registry;
//
//    private KafkaTemplate<String, String> producer;
//
//    @Value("${conf.kafka.ack-topic-name}")
//    private String topicName;
//
//    @BeforeEach
//    void setUp() {
//        MockitoAnnotations.initMocks(this);
//        producer = buildKafkaTemplate();
//
//        // Ждем назначения партиций - иначе Producer может начать попытку записи до готовности брокера
//        for (MessageListenerContainer messageListenerContainer : registry.getListenerContainers()) {
//            ContainerTestUtils.waitForAssignment(messageListenerContainer, embeddedKafkaBroker.getPartitionsPerTopic());
//        }
//    }
//
//    /**
//     * Проверяем, что событие из Кафки получено и передано в Пегу.
//     */
//    @Test
//    void transmittedFromKafkaToPega() throws InterruptedException {
//        // Given
//        NotifyResponsePega okResponseFromPega = new NotifyResponsePega();
//        Message message = new Message();
//        message.setCode("0");
//        message.setSeverity("SUCCESS");
//        List<Message> messageList = new ArrayList<>();
//        messageList.add(message);
//
//        okResponseFromPega.setMessage(messageList);
//
//        String incomingMessage = readFile("4daysFirstMonEnd.json");
//
//        CountDownLatch invocationSignal = new CountDownLatch(1);
//
//        when(restTemplatePega
//                .postForObject(anyString(), anyString(), eq(NotifyResponsePega.class)))
//                .then(invocationOnMock -> {
//                    invocationSignal.countDown();
//                    return okResponseFromPega;
//                });
//
//        // When
//        producer.sendDefault(null, incomingMessage);
//
//        // Then
//        invocationSignal.await(); // Ждем, пока отработает @KafkaListener с бизнес-логикой
//
//        verify(restTemplatePega, times(1))
//                .postForObject(anyString(), eq(incomingMessage), eq(NotifyResponsePega.class));
//    }
//
//    /**
//     * Создаем KafkaTemplate для отправки события в топик
//     */
//    private KafkaTemplate<String, String> buildKafkaTemplate() {
//        Map<String, Object> senderProps = KafkaTestUtils.producerProps(embeddedKafkaBroker);
//        ProducerFactory<String, String> factory = new DefaultKafkaProducerFactory<>(senderProps);
//
//        KafkaTemplate<String, String> kafkaTemplate = new KafkaTemplate<>(factory);
//        kafkaTemplate.setDefaultTopic(topicName);
//
//        return kafkaTemplate;
//    }
//
//    private String readFile(String resourceName) {
//        try {
//            return new String(getClass().getClassLoader().getResourceAsStream(resourceName).readAllBytes());
//        } catch (IOException e) {
//            return "error reading " + resourceName;
//        }
//    }
}
