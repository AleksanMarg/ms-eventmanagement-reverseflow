def service
def version
def image

pipeline {
    agent {
        label 'rs'
    }

    parameters {
        string(name: 'major', description: 'Номер мажора', defaultValue: "${params.major}")
        string(name: 'minor', description: 'Номер минора', defaultValue: "${params.minor}")
        string(name: 'env', description: 'Среда развертывания', defaultValue: "${params.env}")
        string(name: 'gitRepoPath', description: 'Путь к проекту', defaultValue: "${params.gitRepoPath}")
        string(name: 'gitBranchName', description: 'Имя ветки', defaultValue: "${params.gitBranchName}")
        string(name: 'helmChartDir', description: "Относительный путь к папке, в корне которой лежит Chart.yaml", defaultValue: "${params.helmChartDir}")
    }

    environment {
        EMAIL_TO = 'Margaryan.A.Ar@sberbank.ru'
    }

    options {
        buildDiscarder(logRotator(numToKeepStr: '5', daysToKeepStr: '5'))
        timestamps()
    }

    stages {
        stage('Git checkout') {
            steps {
                deleteDir()
                checkout([$class: 'GitSCM', branches: [[name: "${params.gitBranchName}"]], doGenerateSubmoduleConfigurations: false, extensions: [], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'dacd8892-3ba4-4b34-8ef6-5b404295840f', url: "${params.gitRepoPath}"]]])
                sh "echo $JAVA_HOME"
                sh "java -version"
                sh "JAVA_HOME=/usr/lib/jvm/jdk-11.0.4 mvn clean package"
            }
        }

        stage('Build app') {
            steps {
                script {
                    version = "${params.major}.${params.minor}.${BUILD_NUMBER}"
                    println "VERSION = ${version}"
                    sh "mvn versions:set -DnewVersion=${version} -DskipAll -DgenerateBackupPoms=false"
                    sh "mvn -Dmajor.version=${major} -Dminor.version=${minor} -Dbuild.version=${BUILD_NUMBER} package"
                }
            }
        }

        stage('Build docker image') {
            steps {
                script {
                    dir(params.helmChartDir) {
                        chart:
                        {
                            def chart = "./Chart.yaml"
                            def data = readYaml file: chart
                            service = data.name
                            if (!service?.trim()) {
                                error "Chart.name must be not empty"
                            }
                        }
                    }

                    def registry = "https://registry.hub.docker.com/repository/docker/aleksan7/reverse-flow"
                    image = "${registry}/${service}:${version}"

                        sh "docker build -f ${params.helmChartDir}/Dockerfile -t ${image} ."
                        sh "docker push ${image}"

                    def imagesDigest = sh returnStdout: true, script: "docker inspect --format='{{index .RepoDigests 0}}' ${image} | tr -d '\n'"

                    dir(params.helmChartDir) {
                        chart:
                        {
                            def chart = "./Chart.yaml"
                            def data = readYaml file: chart

                            data.annotations.sha256 = imagesDigest.substring(imagesDigest.lastIndexOf(':') + 1)
                            data.version = version
                            data.appVersion = version

                            println("Removing chart file: ${chart}")
                            sh "rm -f ${chart}"
                            writeYaml file: chart, data: data
                        }
                    }
                }
            }
        }

        stage("Helm upgrade") {
            steps {
                script {
                    dir(params.helmChartDir) {
                        println params.env
                        switch (params.env) {
                            case "at": openShiftProjectName = "ci01884453-edevgen-riskstore-dev"; openShiftApiURL = "https://api.dev-gen.sigma.sbrf.ru:6443"; break;
                            case "dev": openShiftProjectName = "ci01884453-edevgen-riskstore-dev"; openShiftApiURL = "https://api.dev-gen.sigma.sbrf.ru:6443"; break;
                            default: openShiftProjectName = "ci01884453-edevgen-riskstore-dev"; openShiftApiURL = "https://api.dev-gen.sigma.sbrf.ru:6443"; break;
                        }

                        println "openShiftProjectName : " + openShiftProjectName
                        println "openShiftApiURL : " + openShiftApiURL
                        withEnv(["PATH+HELM=${tool name: 'helm-v3.3.4', type: 'com.cloudbees.jenkins.plugins.customtools.CustomTool'}"]) {
                            sh "helm template --debug ${service} ./ -f values-dev.yaml"

                            withCredentials([string(credentialsId: "${openShiftProjectName}", variable: 'token')]) {
                                sh "helm upgrade --install ${service} ./ --kube-apiserver=${openShiftApiURL} --atomic --kube-token=${token} --namespace=${openShiftProjectName} -f values-dev.yaml"
                                println "=======LIST======="
                                sh "helm list --kube-apiserver=${openShiftApiURL} --kube-token=${token} --namespace=${openShiftProjectName} --max 30"
                                println "=======HISTORY======="
                                sh "helm history ${service} --kube-apiserver=${openShiftApiURL} --kube-token=${token} --namespace=${openShiftProjectName} --max 30"
                            }
                        }
                    }
                }
            }
        }

        stage('Upload chart to nexus') {
            steps {
                script {
                    dir("${params.helmChartDir}") {
                        def distributive = "${service}-${version}-distrib.zip"
                        def nexus = "nexus.sigma.sbrf.ru/nexus/content/repositories/SBT_CI_distr_repo/ru/sberbank/riskstore/"

                        sh "zip -r ${distributive} ./"
                        sh "ls -lh"
                        withCredentials([usernamePassword(credentialsId: 'dacd8892-3ba4-4b34-8ef6-5b404295840f', passwordVariable: 'nexus_password', usernameVariable: 'nexus_username')]) {
                            sh """curl -k -v -u $nexus_username:$nexus_password --upload-file ${distributive} https://${nexus}/${service}/${version}/${distributive}"""
                        }
                    }
                }
            }
        }
    }   //end stages
    post {
        always {
            cleanWs()
        }
        failure {
            emailext body: 'Check console output at $BUILD_URL to view the results.',
                    to: EMAIL_TO,
                    subject: 'FAILURE ${service}-${version} --> ${env} : $PROJECT_NAME - #$BUILD_NUMBER'
        }
        unstable {
            emailext body: 'Check console output at $BUILD_URL to view the results.',
                    to: EMAIL_TO,
                    subject: 'UNSTABLE ${service}-${version} --> ${env} : $PROJECT_NAME - #$BUILD_NUMBER'
        }
        success {
            emailext body: 'Check console output at $BUILD_URL to view the results.',
                    to: EMAIL_TO,
                    subject: 'SUCCESS ${service}-${version} --> ${env} : $PROJECT_NAME - #$BUILD_NUMBER'
        }
    }
}   //end pipeline