{{ define "kubernetes.labels" }}
app.kubernetes.io/name: {{ .Chart.Name }}
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
app.kubernetes.io/component: application
app.kubernetes.io/part-of: risk-segment
app.kubernetes.io/namespace: {{ $.Release.Namespace }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}
