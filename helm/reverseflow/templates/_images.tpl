{{- define "images.application.sha256" -}}
  {{- $sha256 := required "Необходимо заполнить .Chart.Annotations.sha256" .Chart.Annotations.sha256 -}}
  {{- printf "%s@sha256:%s" .Values.image $sha256 }}
{{- end }}